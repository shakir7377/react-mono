const path = require('path');
const { getLoader, loaderByName } = require('@craco/craco');
const CracoAlias = require("craco-alias");

const packages = [
  path.join(__dirname, '../new'),
  path.join(__dirname, '../common')
];

module.exports = {
  webpack: {
    configure: (webpackConfig, arg) => {
      const { isFound, match } = getLoader(
        webpackConfig,
        loaderByName('babel-loader')
      );
      if (isFound) {
        const include = Array.isArray(match.loader.include)
          ? match.loader.include
          : [match.loader.include];

        match.loader.include = include.concat(packages);
      }
      return webpackConfig;
    }
  },
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        // source: 'tsconfig',
        // baseUrl: '.',
        // tsConfigPath: './tsconfig.path.json',
        aliases:{
          environment: path.join(__dirname, 'src', 'environments', process.env.REACT_APP_ENVIRONMENT ?? "development"),
        }
      },
    }
  ]
};