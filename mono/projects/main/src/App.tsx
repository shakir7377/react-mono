import React, { lazy, Suspense, useEffect } from 'react';
import './App.css';
import {
  Link,
} from "react-router-dom";
import New from '@mono/new/src/App';
import * as constants from '@mono/common/src/constants'
import RoutesEnum from "@mono/common/src/constants/routesEnum"
// import environment from "@mono/common/src/environments/development";
import { useDispatch } from 'react-redux';
import { getAppraisal } from './store/actions/action';
import {Navigations} from '@mono/common/src/navigations'
import {
  BrowserRouter as Router,
  Route,
  Routes,
  useParams
} from "react-router-dom";
// const NewModule = lazy(() => import(/* webpackChunkName: "NewReactApp" */ '@mono/new/src/App'));
//@ts-ignore
import environment from 'environment'

function App() {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(getAppraisal())
  },[])
  
  console.log({environment})  // console.log(environment);
  return (
    <Suspense fallback={ <Loading /> }>
        <Router>
    <div>
      <ul>
        <li>
          <Link to={RoutesEnum.home}>Home {constants.app_name}</Link>
        </li>
        <li>
          <Link to={RoutesEnum.about}>About</Link>
        </li>
        <li>
          <Link to={RoutesEnum.new}>New React</Link>
        </li>
      </ul>
      <Navigations />
    </div>
    </Router>
        </Suspense>
  );
}

export const MainApp = () => <div className="">MainApp</div>
// const NewModule = () => <div className="">NewModule</div>

const Loading = () => <div className="">Loading</div>

export default App;
