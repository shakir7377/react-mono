import Http from '@mono/common/src/services/http/http';
import environment from 'environment'
import types from '../types';
import Action from "@mono/common/src/services/http/actionUtility";
import HttpErrorResponseModel from '@mono/common/src/services/http/httpErrorResponseModel'

export const getAppraisal = () => async (dispatch:any) =>{
    const res = Http.get(environment.api.v1.appraisals.appraisal);
    const actionConfig = {
        dispatch,
        actionType: types.GET_APPRAISALS,
        effect: res,
        isLoading: true,
    };
    await Action.createThunkEffect(actionConfig);
}