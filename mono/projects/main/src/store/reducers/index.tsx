import { Reducer } from 'redux';
import {appraisalReducer,AppraisalState} from './appraisalReducer';

interface reducerProps{
    appraisalReducer:Reducer<AppraisalState>
}

export const reducers:reducerProps = {
    appraisalReducer
}

export default reducers;