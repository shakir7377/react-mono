import types from "../types";
import { Action, Reducer } from 'redux';

export interface AppraisalState {
    loading:boolean,
    appraisals:Array<any>,
}

const initialState:AppraisalState = {
    loading: false,
    appraisals:[],
};

export interface ActionType {
    payload:any,
    loadingType:string
    type:string,
    
}

export const appraisalReducer:Reducer<AppraisalState> = (state:AppraisalState | undefined,incomingAction:Action):AppraisalState =>{
    if (state === undefined) {
        return initialState;
    }
    const action = incomingAction as ActionType;
    switch (action.type) {
        case types.GET_APPRAISALS:
            return {
                ...state,
                appraisals: action.payload
            };
        default:
            return state;
    }
}

export default appraisalReducer;