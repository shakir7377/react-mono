declare module 'environment' {
  import baseEnv from '@mono/common/src/environments/base';
  // import baseEnv from 'environments/base';
  const value: ReturnType<typeof baseEnv>;

  export default value;
}
