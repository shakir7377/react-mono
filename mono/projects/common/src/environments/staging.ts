import environment from './base';

/*
 * base.ts is the default environment for production.
 * You shouldn't have override anything.
 */

const baseApi = '/';
const env = environment(baseApi);

const stagingEnv = {
  ...env,
  api: {
    ...env.api,
  },
  isProduction: false,
  isStaging: true,
  isDevelopment: false,
  isTesting: false,
};

export default stagingEnv;
