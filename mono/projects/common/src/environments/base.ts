import AppraisalsAPIs from './endpoints/v1/appraisal';

let base = (baseApi:string) => {
  let v1 = `/v1`;
  return {
    route: {
      baseRoute: baseApi, 
    },

    api: {
      v1:{
        appraisals:AppraisalsAPIs,
      },      
    },
    isProduction: true,
    isDevelopment: false,
    isStaging: false,
    isTesting: false,
  };
}



export default base;