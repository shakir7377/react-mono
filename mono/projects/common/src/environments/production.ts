import environment from './base';

/*
 * base.ts is the default environment for production.
 * You shouldn't have override anything.
 */

const baseApi = '/';
const env = environment(baseApi);

const productionEnv = {
  ...env,
  api: {
    ...env.api,
  },
  isProduction: true,
  isStaging: false,
  isDevelopment: false,
  isTesting: false,
};

export default productionEnv;
