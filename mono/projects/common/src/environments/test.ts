import environment from './base';

const baseApi = '/';
const env = environment(baseApi);

const testEnv = {
  ...env,
  api: {
    ...env.api,
  },
  isProduction: false,
  isStaging: false,
  isDevelopment: true,
  isTesting: true,
};

export default testEnv;
