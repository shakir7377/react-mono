import environment from './base';

const baseApi = '/';
const env = environment(baseApi);

const developmentEnv = {
  ...env,
  api: {
    ...env.api,
  },
  isProduction: false,
  isStaging: false,
  isDevelopment: true,
  isTesting: false,
};

export default developmentEnv;
