interface useFormProps {
  control?: any;
  register?: any;
  setError?: any;
  setValue?: any;
  getValues?: any;
  trigger?: any;
  watch?: any;
  handleSubmit?: any;
  rules?: any;
  defaultValue?: any;
  formError?: any;
}

export interface basicInputInterface extends useFormProps {
  name: string;
  value?: any;
  onChange?: Function;
  label?: any;
  placeholder?: string;
  size?: string;
  width?: string;
  disabled?: boolean;
  onKeyUp?: Function;
  onKeyPress?: Function;
  onBlur?: Function;
  options?: any[];
  error?: any;
  className?: string;
  isMultiple?: boolean;
  autoComplete?: any;
  rows?: any;
  cols?: any;
}

export interface checkBoxListInterface extends basicInputInterface {
  selectedItems?: any[];
  items?: any[];
}
export interface otherInputInterface extends basicInputInterface {
  minValue?: string;
  maxValue?: string;
  type?: string;
  autoFocus?: boolean;
  innerLabel?: string;
  rightSideText?: string;
  enableClearText?: boolean;
  enableOkButton?: boolean;
  onClearText?: Function;
  hideAction?: boolean;
}

export interface otherTimePickerInterface extends basicInputInterface {
  time?: string;
  defaultMeridiem?: string;
}
export interface reactSelectInterface extends basicInputInterface {
  isArray?: boolean;
  isArrayKeys?: boolean;
  isAbove?: boolean;
  multiple?: boolean;
  loading?: boolean;
  isClearable?: boolean;
  loadingType?: string;
  components?: any;
}

export interface checkRadioInterface extends basicInputInterface {
  checked?: boolean;
  labelPosition?: string;
}

export interface dateRangeInterface extends otherInputInterface {
  startDate?: any;
  endDate?: any;
}

export interface textEditorInterface extends basicInputInterface {
  toolbarId?: string;
}

export interface singleDocsInterface extends basicInputInterface {
  btnText?: string;
  extraClassName?: string;
}

export interface ydmInputCommonInterface extends basicInputInterface {
  monthValue?: string | number;
  yearValue?: string | number;
  dayValue?: string | number;
  inputShow?: any;
  // inputShow?:string[] | 'all' | 'months' | 'years' | 'days',
}

export interface ydmWholeInterface extends ydmInputCommonInterface {
  value: ydmValueProps;
  inputShow?: string[] | "all" | "months" | "years" | "days";
}

interface ydmValueProps {
  month?: string | number;
  year?: string | number;
  day?: string | number;
}

export interface checkdGroupProps extends basicInputInterface {
  checkGroupArray?: any[];
  checkedGroupValue?: any[];
}

export interface wholeFormInterface
  extends singleDocsInterface,
    checkRadioInterface,
    reactSelectInterface,
    otherInputInterface,
    textEditorInterface,
    dateRangeInterface,
    checkdGroupProps,
    checkBoxListInterface,
    ydmInputCommonInterface {
  formName:
    | "textinput"
    | "textarea"
    | "stepper"
    | "reactselect"
    | "checkBoxGroup"
    | "dateinput"
    | "customdateinput"
    | "chipsinput"
    | "singledocumentinput"
    | "checkgroup"
    | "asyncautoselect"
    | "radioList"
    | "reactdaterange"
    | "reactcalendar"
    | "searchInput"
    | "timepicker"
    | "radiogroup"
    | "ydmInput"
    | "uploadInput"
    | "selectInput"
    | "texteditor"
    | "dragAndDropUpload"
    | "switch"
    | "dragAndDropUpload"
    | "hourMinute"
    | "checkBoxList";
  alignment?: string;
  stepperType?: any;
  validators?: any;
  chipsArray?: any;
  isNepali?: boolean;
  closeOnClick?: boolean;
  id?: string;
  badgeColor?: string;
  isDateRange?: boolean;
  minDate?: string;
  maxDate?: string;
  isFullDetails?: boolean;
  isTextInNepali?: boolean;
  isStatus?: boolean;
  leftLabel?: any;
  rightLabel?: any;
  uploadSize?: any;
  maxFiles?: any;
  loadOptions?: Function;
  hide?: Boolean;
}
