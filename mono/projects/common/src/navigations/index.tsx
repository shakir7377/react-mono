import {
    BrowserRouter as Router,
    Route,
    Routes,
    Link,
    useParams
} from "react-router-dom";
import React, { lazy, Suspense } from 'react';
import RoutesEnum from '../constants/routesEnum';
const NewModule = lazy(() => import(/* webpackChunkName: "NewReactApp" */ '@mono/new/src/App'));
const MainApp = lazy(() => import(/* webpackChunkName: "NewReactApp" */ '@mono/main/src/scenes/MainApp'));

export const Navigations = () => {
    return(
        <Routes>
            <Route path={RoutesEnum.about} element={<About />} >
            </Route>
            <Route path={RoutesEnum.home} element={<MainApp />}>
            </Route>
            <Route path={RoutesEnum.new} element={<NewModule />}>
            </Route>
        </Routes>
    )
}


const About = () => <div className="">About</div>

const Loading = () => <div className="">Loading</div>