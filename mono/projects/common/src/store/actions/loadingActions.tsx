
import types from "../../constants/types";

export const addLoading = (data:any,type:string="normal"):any => (dispatch:any):any => {
  dispatch({
    type: types.ADD_LOADING,
    payload:data,
    loadingType:type
  });
};
