import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { History } from "history";
// import { reducerss } from "../reducers";
import reducers from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";
import reduxFreeze from "redux-freeze";
import environment from "../environments/development"
import { createBrowserHistory } from "history";
import AppraisalReducer from '@mono/main/src/store/reducers'

const initialState = {};

const history = createBrowserHistory();

const enhancers = [];
// const windowIfDefined = typeof window === "undefined" ? null : (window as any);
// if (environment.isDevelopment && windowIfDefined && windowIfDefined.__REDUX_DEVTOOLS_EXTENSION__) {
//   enhancers.push(windowIfDefined.__REDUX_DEVTOOLS_EXTENSION__());
// }
const middleware = [
  // environment.isDevelopment ? reduxFreeze : null,
  reduxFreeze,
  thunk,
  routerMiddleware(history),
//   checkTokenExpirationMiddleware,
  // errorToastMiddleware()
].filter(Boolean);
const rootReducer = combineReducers({
  ...reducers,
  ...AppraisalReducer,
  router: connectRouter(history),
});

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
