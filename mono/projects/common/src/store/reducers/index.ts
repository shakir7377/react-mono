import { Reducer } from 'redux';
import {loadingReducer} from './loadingReducer';

// interface reducerProps{
//     loadingReducer:Reducer<LoadingInitialState>
// }

export const reducers = {
    loadingReducer
}

export default reducers;