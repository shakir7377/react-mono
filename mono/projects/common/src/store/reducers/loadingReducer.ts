import types from "../../constants/types";
import { Action, Reducer } from 'redux';

// export interface LoadingInitialState {
//     loading:boolean,
//     loadingType:string,
//     refresh_loading:boolean
// }

const initialState = {
    loading: false,
    loadingType: '',
    refresh_loading:false,
};

// export interface ActionType {
//     payload:boolean,
//     loadingType:string
//     type:string,
    
// }

//@ts-ignore
export const loadingReducer = (state,incomingAction) =>{
    if (state === undefined) {
        return initialState;
    }
    const action = incomingAction;
    switch (action.type) {
        case types.ADD_LOADING:
            return {
                ...state,
                loading: action.payload,
                loadingType:action.loadingType
            };
        case types.ADD_REFRESH_LOADING:
            return{
                ...state,
                refresh_loading:action.payload
            }
        default:
            return state;
    }
}

export default loadingReducer;