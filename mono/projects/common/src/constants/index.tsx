export const app_name = "React Demo";

export const AuthToken = "AuthToken";
export const RefreshToken = "RefreshToken";
export const IdleTimeOut = "IdleTimeOut";
export const ExpiresMinutes = "ExpiresMinutes";
export const Expires = "Expires";
export const UserDetail = "UserDetail";