import {
    AuthToken,
    RefreshToken,
    IdleTimeOut,
    ExpiresMinutes,
    Expires,
    UserDetail
} from '../constants'
import store from "../store/store";

interface TokenInterface{
    saveAllToken:Function,
    clearAllToken:Function,
    adduserDetails:Function,
    getAuthToken:Function,
    getExpiresMinutes:Function,
    getRefreshToken:Function,
    getIdleTimeOut:Function,
    getUserDetail:Function,
    getExpires:Function,
}

class TokenService{
    saveAllToken(
        {
            AuthTokenVal,
            RefreshTokenVal,
            IdleTimeOutVal,
            ExpiresMinutesVal,
            ExpiresVal,
        }:any
    ){
        localStorage.setItem(ExpiresMinutes, ExpiresMinutesVal);
        // localStorage.setItem(AuthToken, AuthTokenVal);
        localStorage.setItem(RefreshToken, RefreshTokenVal);
        localStorage.setItem(IdleTimeOut, IdleTimeOutVal);
        localStorage.setItem(Expires, ExpiresVal);
    }
    clearAllToken(){
        localStorage.setItem(ExpiresMinutes, "");
        localStorage.setItem(AuthToken, "");
        localStorage.setItem(RefreshToken, "");
        localStorage.setItem(IdleTimeOut, "");
        localStorage.setItem(UserDetail, "");
        localStorage.setItem(Expires, "");
    }
    adduserDetails(userDetails:any){
        localStorage.setItem(UserDetail, userDetails);
    }
    // getAuthToken = () => localStorage.getItem(AuthToken);
    getAuthToken = () => {
        const state:any = store.getState();
        return state?.auth?.token
    };
    updateAuthToken = (token:any) => localStorage.setItem(AuthToken,token);
    getExpiresMinutes = () => localStorage.getItem(ExpiresMinutes);
    getRefreshToken = () => localStorage.getItem(RefreshToken);
    getIdleTimeOut = () => localStorage.getItem(IdleTimeOut);
    getUserDetail = () => localStorage.getItem(UserDetail);
    getExpires = () => localStorage.getItem(Expires);
}


export default new TokenService();