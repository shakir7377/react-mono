import types from "../../constants/types";
import HttpErrorResponseModel from "./httpErrorResponseModel";
import DataFormator from "../../helpers/dataFormator";

interface actionProps {
  dispatch: any;
  actionType: string;
  effect: any;
  type?: string;
  message?: string;
  id?: string | number;
  isSelect?: boolean;
  isValueKey?: boolean;
  isLoading?: boolean;
  isJustData?: boolean;
  extraPayload?: {};
}

export default class ActionUtility {
  static async createThunkEffect({
    dispatch,
    isValueKey,
    extraPayload,
    actionType,
    isSelect,
    isJustData,
    effect,
    id,
    message,
    type,
    isLoading = false,
  }: actionProps) {
    // if(type === 'get') dispatch(ActionUtility.createAction(actionType));
    if (isLoading) dispatch({type: types.ADD_LOADING, payload: true});
    const model = await effect;

    if (isLoading) dispatch({type: types.ADD_LOADING, payload: false});
    const isError = model instanceof HttpErrorResponseModel;
    if (isError) {
      dispatch({type: `types.GET_ERRORS`, payload: model});
      return model.errors;
    }
    if (!model?.data?.Status && model?.data?.Message) {
      dispatch(toastMessage({message: model.data.Message, type: model.data.MessageType}));
    }
    if (model?.data?.Message?.length > 0) {
      dispatch(toastMessage({message: model.data.Message, type: model.data.MessageType}));
    }
    
    if (!Boolean(actionType)) {
      return model.data;
    }

    if (isSelect) {
      dispatch(
        ActionUtility.createAction(
          actionType,
          DataFormator(isJustData ? model.data : model.data.Data, isValueKey),
          isError,
          extraPayload
        )
      );
      return model.data;
    }
    if (isJustData) {
      dispatch(ActionUtility.createAction(actionType, model.data, isError, extraPayload));
      return model.data;
    }
    if (type === "get")
      dispatch(ActionUtility.createAction(actionType, model.data, isError, extraPayload));
    if (!model.data.Status) {
      dispatch({type: types.GET_ERRORS, payload: model.data.Message});
      return model.data;
    }

    if (type === "delete") {
      dispatch(ActionUtility.createAction(actionType, id, isError, extraPayload));
    } else {
      dispatch(
        ActionUtility.createAction(actionType, model.data.Data, isError, extraPayload)
      );
    }
    if (message) {
      dispatch(toastMessage({message, type: "success"}));
    }
    dispatch({type: types.CLEAR_ERRORS});
    return model.data;
  }
  static createAction(type:string, payload:any = null, error:any = false, extraPayload:any = null) {
    return extraPayload
      ? {type, payload, error, ...extraPayload}
      : {type, payload, error};
  }
}

export const toastMessage = (data:any) => (dispatch:any) => {
  let MessageType = data.type ? data.type : null;
  let Message = data.message ? data.message : [];

  dispatch({
    type: types.ADD_MESSAGE,
    payload: {type: MessageType, message: Message},
  });
};
