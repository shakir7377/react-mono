import React from 'react';
import './App.css';
import environment from 'environment'

function App() {
  console.log({environment})
  return (
    <div className="App">
      New Appraisal App
    </div>
  );
}

export default App;
